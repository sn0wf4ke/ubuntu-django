# django package installation comparision

## Dockerfile.apt

* using same package names like for ubuntu 18.04
* packages not found: libjs-sinon

## Dockerfile.pip

* using last available compatible version from pip as found in ubuntu 20.04 apt repository
* pygraphviz is not able to resolve dependencies for graphviz-dev installation
